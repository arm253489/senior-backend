const { Model, DataTypes } = require('sequelize');

class UserCheckin extends Model {}

module.exports = {
  init: function(sequelize) {
    UserCheckin.init({
      
    }, {
      sequelize,
      underscored: true,
      paranoid: true,
      charset: "utf8mb4",
      collate: "utf8mb4_unicode_ci",
    });
  },
  model: UserCheckin,
};
