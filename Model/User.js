const { Model, DataTypes } = require('sequelize');
const user = require('../routes/user');

class User extends Model {}

module.exports = {
  init: function(sequelize) {
    User.init({
      Fullname: {
        allowNull: false,
        type: DataTypes.STRING,
        
      },
      Phonenumber: {
        type: DataTypes.STRING,
        allowNull: false,
        
      },
      BluetoothMac: {
        type: DataTypes.STRING,
        allowNull: false,
        primaryKey:true
      },
      Status: {
        type: DataTypes.STRING,
        allowNull: false
      },
      BluetoothScanDevice: {
        type: DataTypes.STRING,
        allowNull: true
      },
      Location: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      LocationDevicefound: {
        type: DataTypes.STRING,
        allowNull: true,
        unique: true
      }

    }, {
      sequelize,
      // modelName: 'user',
      underscored: true,
      paranoid: false,
      charset: "utf8mb4",
      collate: "utf8mb4_unicode_ci",
    });
  },
  model: User
};
