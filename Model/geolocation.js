const { Model, DataTypes } = require('sequelize');

class GeoLocation extends Model {}

module.exports = {
  init: function(sequelize) {
    GeoLocation.init({

      // Latitude: {
      //   allowNull: false,
      //   type: DataTypes.STRING
      // },
      // Longitude {
      //   allowNull: false,
      //   type: DataTypes.STRING
      // },
    
      
    }, {
      sequelize,
      underscored: true,
      paranoid: true,
      charset: "utf8mb4",
      collate: "utf8mb4_unicode_ci",
    });
  },
  model: GeoLocation,
};
