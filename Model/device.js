const { Model, DataTypes } = require('sequelize');

class Device extends Model {}

module.exports = {
  init: function(sequelize) {
    Device.init({
      DeviceName: {
        allowNull: false,
        type: DataTypes.STRING
      },
      DeviceMacAddress: {
        type: DataTypes.STRING,
        primaryKey:true,
        allowNull: false,
      },
     
    }, {
      sequelize,
      // modelName: 'device',
      underscored: true,
      paranoid: true,
      charset: "utf8mb4",
      collate: "utf8mb4_unicode_ci",
    });
  },
  model: Device,
};
