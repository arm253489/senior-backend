const { Model, DataTypes } = require('sequelize');

class Location extends Model {}

module.exports = {
  init: function(sequelize) {
    Location.init({
      id: {
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
        type: DataTypes.INTEGER,
      },
      LocationName: {
        allowNull: false,
        type: DataTypes.STRING
      },
      Latitude: {
        allowNull: false,
        type: DataTypes.STRING
      },
      Longitude: {
        allowNull: false,
        type: DataTypes.STRING
      },
      Status: {
        type: DataTypes.STRING,
        allowNull: false
      }
      
    }, {
      sequelize,
      // modelName: 'location',
      underscored: true,
      paranoid: true,
      charset: "utf8mb4",
      collate: "utf8mb4_unicode_ci",
    });
  },
  model: Location,
};
