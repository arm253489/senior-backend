const { Model, DataTypes } = require('sequelize');

class DeviceLocation extends Model {}

module.exports = {
  init: function(sequelize) {
    DeviceLocation.init({
    }, {
      sequelize,
      underscored: true,
      paranoid: false,
      charset: "utf8mb4",
      collate: "utf8mb4_unicode_ci",
    });
  },
  model: DeviceLocation,
};
