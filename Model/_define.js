
const User = require('./user');
const Device = require('./device');
const Location = require('./location');
const UserCheckin = require('./userCheckin');
const DeviceLocation = require('./deviceLocation');
const GeoLocation = require('./geolocation');


module.exports = function(sequelize) {
  User.init(sequelize);
  Device.init(sequelize);
  Location.init(sequelize);
  UserCheckin.init(sequelize);
  DeviceLocation.init(sequelize);
  GeoLocation.init(sequelize);
 
// Many to Many User and Location 
User.model.belongsToMany(Location.model,{through : UserCheckin.model});
Location.model.belongsToMany(User.model,{through : UserCheckin.model});

//Many to Many Device and Location 
Device.model.belongsToMany(Location.model,{through : DeviceLocation.model, foreignKey: { name: "DeviceMacAddress"}});
Location.model.belongsToMany(Device.model,{through : DeviceLocation.model});

//One to One GeoLocation and Location

Location.model.hasOne(GeoLocation.model);
GeoLocation.model.belongsTo(Location.model);

}

