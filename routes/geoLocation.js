const express = require("express");
const router = express.Router();


module.exports = function (gr) {
    router.get('/', gr.listGeoLocation);
    router.post('/', gr.createGeoLocation);
    router.put('/',gr.updateGeoLocation);

    return router;


}
