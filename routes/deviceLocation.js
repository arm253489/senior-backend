const express = require("express");
const router = express.Router();


module.exports = function (dl) {
    router.get('/findByLocationId/:id', dl.findByLocationID);
    router.get('/findByDeviceMacAdress/:id', dl.findByDeviceMacAdress);
    router.post('/', dl.createDeviceCheck);
    router.get('/', dl.findAll);
    router.delete('/:id',dl.DeleteByDeviceMac);
    
  



    return router;


}