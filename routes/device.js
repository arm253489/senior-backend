const express = require("express");
const router = express.Router();


module.exports = function (dv) {
    router.get('/', dv.listDevice);
    router.post('/', dv.createDevice);
    router.put('/:id', dv.updateDevice);



    return router;


}
