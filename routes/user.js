const express = require("express");
const router = express.Router();

module.exports = function(cc) {
  router.get('/', cc.listUser);
  router.post('/', cc.createUser);
  router.put('/', cc.updateUser);
  router.delete('/:id', cc.DeleteUser);
  router.get('/:id', cc.FindUserbyBluetoothMac);



  return router;
}
