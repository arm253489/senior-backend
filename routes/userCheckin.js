const express = require("express");
const router = express.Router();


module.exports = function (ucc) {
    router.get('/findByLocationId/:id', ucc.findByLocationID);
    router.post('/', ucc.createUserCheck);
    router.get('/findByBluetooth/:id', ucc.findByBluetoothMac);
    



    return router;


}