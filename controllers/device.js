const Device = require('../Model/device');
const DeviceLocation = require('../Model/deviceLocation')
const Location = require('../Model/location')

class DeviceController {
  constructor(sequelize) {
    this.sequelize = sequelize;
  }

  async listDevice(req, res, next) {
    const devices = await Device.model.findAndCountAll();
    //const locations = await Location.model.findAndCountAll();
    res.status(200).json({
      count: devices.count,
      rows: devices.rows
    });
  }
  // async DeleteDevice(req, res, next) {
  //   const { id } = req.params;
  //     const device = await Device.model.destroy({where : {DeviceMacAddress:id}});
  //      res.status(201).json(device);
  //   }

  async createDevice(req, res, next) {
    try {

      let Locationdata = { LocationName: req.body.LocationName, Latitude: req.body.Latitude, Longitude: req.body.Longitude, Status: req.body.Status }
      let Devicedata = { DeviceName: req.body.DeviceName, DeviceMacAddress: req.body.DeviceMacAddress };
     
      
      const device = await Device.model.create(Devicedata);
      const location = await Location.model.create(Locationdata);
      res.status(201).json(device.toJSON());
      console.log("Location id = " + location.toJSON().id , "DeviceMac = " +  device.toJSON().DeviceMacAddress)
      
      
      
      let DeviceLocationdata = {LocationId : location.toJSON().id , DeviceMacAddress: device.toJSON().DeviceMacAddress} ;
      console.log("Location id = " + location.toJSON().id , "DeviceMac = " +  device.toJSON().DeviceMacAddress)
      const devicelocation = await DeviceLocation.model.create(DeviceLocationdata);
   
      // res.status(201).json(location.toJSON());
      // res.status(201).json(devicelocation.toJSON());
      
    
  
      console.log(Devicedata);
      console.log(Locationdata);
      console.log(DeviceLocationdata);
    } catch (error) {
      console.log(error);
    }




  }

  async updateDevice(req, res, next) {
    // const deviceUpdate = await Device.model.update(req.body, { where: { DeviceMacAddress: req.body.DeviceMacAddress } });
    // let DeviceMacAdress = req.body.DeviceMacAdress;
    // const data = await Device.model.findOne({ where: { DeviceMacAdress } });
    // console.log(data.toJSON());
    // res.status(200).json(data.toJSON());
    const { id } = req.params;
    try{
      let Locationdata = { LocationName: req.body.LocationName, Latitude: req.body.Latitude, Longitude: req.body.Longitude, Status: req.body.Status }
      let Devicedata = { DeviceName: req.body.DeviceName, DeviceMacAddress: req.body.DeviceMacAddress };

      console.log(Locationdata);
      console.log(Devicedata);

      const updateDeviec = await Device.model.update(Devicedata,{where:{DeviceMacAddress:req.body.DeviceMacAddress}})
      const updateLocation = await Location.model.update(Locationdata,{where:{id:id}})
      
      res.status(200).json({"message":"success"})
    }catch(error){
      console.log(error);
    }
    // console.log(req.body);
    
  }



}

module.exports = DeviceController;
