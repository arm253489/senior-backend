const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");


class AdminAuthController {
    constructor(sequelize) {
        this.sequelize = sequelize;
    }

    async login(req, res, next) {
        const { password } = req.body;
        try {
            if (password == "1234") {
                res.status(200).json({
                    token: jwt.sign(
                        {
                            access: true
                        },
                        process.env.JWT_SECRET_KEY,
                        {
                            expiresIn: "8h",
                        }
                    ),
                });
            }
            else {
                res.sendStatus(401);
                return;
            }
        } catch (error) {
            console.log(error)
            next(error);
        }
    }
    async Checkauth(req, res, next) {
        res.status(201).json({
            message: "Admin login sucess!!"
        });
    }

}

module.exports = AdminAuthController;