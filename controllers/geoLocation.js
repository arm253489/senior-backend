const GeoLocation = require('../Model/geolocation');

class GeoLocationController {
  constructor(sequelize) {
    this.sequelize = sequelize;
  }

  async listGeoLocation(req, res, next) {
    const geolocations = await GeoLocation.model.findAndCountAll();
    res.status(200).json({
      count: geolocations.count,
      rows: geolocations.rows
    });
  }

  async createGeoLocation(req, res, next) {
    const geolocation = await GeoLocation.model.create(req.body);
    
    res.status(201).json(geolocation.toJSON());
    console.log(geolocation);
    
  }

  async updateGeoLocation(req, res, next) {
    const geolocation = await GeoLocation.model.update(req.body, { where: { locationId: req.body.locationId } });
    let locationId = req.body.locationId;
    const data = await GeoLocation.model.findOne({ where: {locationId}});
    console.log(data.toJSON());
      res.status(200).json(data.toJSON());
  }
  



}

module.exports = GeoLocationController;
