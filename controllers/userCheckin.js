const UserCheckin = require('../Model/userCheckin');

class UserCheckinController {
  constructor(sequelize) {
    this.sequelize = sequelize;
  }

  async findByLocationID(req, res, next) {
    const { id } = req.params;
    const findLocationID = await UserCheckin.model.findAll({where : {locationId : id}});
    console.log(findLocationID.rows);
    res.status(200).json(findLocationID)
   
  }

  async findByBluetoothMac(req, res, next) {
    const { id } = req.params;
      const findBluetoothMac = await UserCheckin.model.findAll({ where: { userBluetoothMac: id }});
      res.status(200).json(findBluetoothMac);
   
    // console.log(findBluetoothMac.rows);


}
async createUserCheck(req, res, next) {
  const { UserBluetoothMac, LocationID } = req.body;
  try {
    const userCheckin = await UserCheckin.model.create({ userBluetoothMac :UserBluetoothMac, locationId: LocationID });
    // Userdatacheck.create({LocationID:Location.id , UserBluetooth:User.BluetoothMac})
    res.status(201).json(userCheckin.toJSON());
    
  } catch (error) {
    next(error);
  }
}
}

module.exports = UserCheckinController;
