const DeviceLocation = require('../Model/deviceLocation');
const Location = require('../Model/location');
const Device = require('../Model/device');


class DeviceLocationController {
  constructor(sequelize) {
    this.sequelize = sequelize;
  }

  async findByLocationID(req, res, next) {
    const { id } = req.params;
    const findLocationID = await DeviceLocation.model.findAll({where : {LocationId :id}});
    console.log(findLocationID.rows);
    res.status(200).json(findLocationID)
   
  }

  async findByDeviceMacAdress(req, res, next) {
    const { id } = req.params;
    const findDevicehMac = await DeviceLocation.model.findAll({where:{DeviceMacAddress :id }});
    console.log(findDevicehMac.rows);
    res.status(200).json(findDevicehMac);

}
async DeleteByDeviceMac(req, res, next) {
  const { id } = req.params;
  const findData = await DeviceLocation.model.findOne({where: {DeviceMacAddress:id}});
  const findLocationID = await DeviceLocation.model.destroy({where : {DeviceMacAddress : id}});
  const deleteDevice = await Device.model.destroy({where: {DeviceMacAddress:findData.DeviceMacAddress}},{truncate: true});
  const deleteLocation = await Location.model.destroy({where: {id:findData.LocationId}},{truncate: true}); 
  console.log(findData);
  res.status(200).json({message: 'Success!!!'})
 
}


async findAll(req, res, next) {
  const findall = await Device.model.findAndCountAll({
    attributes: ["DeviceName", "DeviceMacAddress"],
    include: [
      {
        model: Location.model,
        attributes: ["id","LocationName", "Latitude", "Longitude", "Status"],
        through: {attributes: []}
      },
    ]
  });
  console.log(findall);
  
  res.status(200).json({
    count: findall.count,
    rows: findall.rows
  });
}
async createDeviceCheck(req, res, next) {
  
  const { DeviceMacAddress, LocationID } = req.body;
    // const CreateDeviceCheck = await DeviceLocation.model.create({ deviceMacAdress : DeviceMacAdress, locationId: LocationID });
    const CreateDeviceCheck = await DeviceLocation.model.create({DeviceMacAddress : DeviceMacAddress , LocationId : LocationID});
    res.status(201).json(CreateDeviceCheck.toJSON());
    
  
}
}

module.exports = DeviceLocationController;
