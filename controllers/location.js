const Location = require('../Model/location');

class LocationController {
  constructor(sequelize) {
    this.sequelize = sequelize;
  }

  async listLocation(req, res, next) {
    const locations = await Location.model.findAndCountAll();
    res.status(200).json({
      count: locations.count,
      rows: locations.rows
    });
  }

  async createLocation(req, res, next) {
    const location = await Location.model.create(req.body);
    res.status(201).json(location.toJSON());
  }

  



}

module.exports = LocationController;
