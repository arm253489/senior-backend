const User = require('../Model/user');

class UserController {
  constructor(sequelize) {
    this.sequelize = sequelize;
  }

  async listUser(req, res, next) {
    const users = await User.model.findAndCountAll();
    res.status(200).json({
      count: users.count,
      rows: users.rows
    });
  }

  
  async FindUserbyBluetoothMac(req, res, next) {
    const { id } = req.params;
    const users = await User.model.findAll({where:{BluetoothMac : id}});
    console.log(users.rows);
    res.status(200).json(users);
  }

  async createUser(req, res, next) {
    const user = await User.model.create(req.body);
    res.status(201).json(user.toJSON());
  }

  async updateUser(req, res, next) {
    const user = await User.model.update(req.body, { where: { BluetoothMac: req.body.BluetoothMac } });
    let BluetoothMac = req.body.BluetoothMac;
    const data = await User.model.findOne({ where: {BluetoothMac}});
    console.log(data.toJSON());
      res.status(200).json(data.toJSON());
  }
  
  async DeleteUser(req, res, next) {
  const { id } = req.params;
    const user = await User.model.destroy({where : {BluetoothMac:id}});
     res.status(201).json(user);
  }
  

}

module.exports = UserController;
