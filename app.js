//Load environment
require("dotenv").config();

const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const db = require("./database");
require("./Model/_define")(db);

const UserController = require("./controllers/User");
const AdminAuthController = require("./controllers/adminauth")
const DeviceController = require("./controllers/device")
const LocationController = require("./controllers/location")
const UserCheckinController = require('./controllers/userCheckin');
const DeviceLocationController = require('./controllers/deviceLocation');
const GeoLocationController = require("./controllers/geoLocation");


// Instanciate controller
const uc = new UserController(db);
const aa = new AdminAuthController(db)
const dv = new DeviceController(db)
const lt = new LocationController(db)
const ucc = new UserCheckinController(db)
const dl = new DeviceLocationController(db)
const gr = new GeoLocationController(db)
const userRoute = require('./routes/user')(uc);
const adminauthRoute = require('./routes/adminauth')(aa);
const deviceRoute = require('./routes/device')(dv);
const locationRoute = require('./routes/location')(lt);
const userCheckinRoute = require('./routes/userCheckin')(ucc);
const deviceLocationRoute = require('./routes/deviceLocation')(dl);
const geoLocationRoute = require('./routes/geoLocation')(gr);


//Parsing JSON body
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Use logging
morgan('dev');

//Handle CORS
const corsOptions = {
  origin: "*",
  methods: "GET,PUT,PATCH,POST,DELETE",
  preflightContinue: false,
  optionsSuccessStatus: 204,
  allowedHeaders: ["Authorization", "Content-Type"],
}
app.use(cors(corsOptions));

//Set up main routes
app.use('/users', userRoute);
app.use('/adminlogin',adminauthRoute);
app.use('/devices',deviceRoute);
app.use('/locations',locationRoute);
app.use('/usercheckin',userCheckinRoute);
app.use('/deviceLocation',deviceLocationRoute);
app.use('/geoLocations',geoLocationRoute);


//Handle error 
app.use((req, res, next) => {
  const error = new Error("Not found");
  error.status = 404;
  next(error);
});
app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message,
    }
  });
});

//Close database connection when program is terminated
process.on("exit", () => {
  db.close();
});

//Start server
app.listen(process.env.PORT || 8080);
